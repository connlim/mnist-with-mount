# Distributed MNIST with Tensorflow and DC/OS

All configs in this repo download the run files from the repo through the Internet. __The repo URL is hardcoded.__ If using in another repo be sure to change the URL and folder to that of the new repo.

If the program doesn't specify to use CPU or GPU, then it will use CPU only on CPU workers and CPU+GPU on GPU workers. There is no way for Tensorflow to use the GPU only as some functions are not implemented for GPUs.

## DC/OS Tensorflow Package

All files related to the Tensorflow package can be found in `dcos_tensorflow_package`.

All  python files in this folder use the `main(server, log_dir, context)` definition for the main function as this is how the DC/OS Tensorflow package works. `server` is the Tensorflow server object, `log_dir` is the file path to the log directory, and `context` contains the program context options.

Program-specific options are set using the `job_context` parameter in the JSON config. The program can retrieve the options using the `context.get('option_name')` function.

See the following for more information:  
<https://github.com/dcos-labs/dcos-tensorflow-tools>  
<https://mesosphere.github.io/dcos-tensorflow/>  
<https://mesosphere.com/blog/tensorflow-custom-model/>

### Overview of important config options

Option | Description
---    | ---
`job_url` | URL to download the program from. Archives will be extracted automatically. Typically just use the master zip from an online repo.
`job_path` | Path to the folder containing the job file. Note: if a zip was downloaded using `job_url`, the extracted files will be in `./<zip_name>/`
`job_name` | The name of the Python file to run. Do not include the file extension. (E.g. `mnist_with_mount.py` is just `mnist_with_mount`)
`job_context` | String of dictionary of context settings that can be used in the program. (E.g. `{"learning_rate":0.001, "max_steps":100000, "run_name": "demo"}`)
`gcs_key` | Service Account key for Google Cloud Storage (optional). _Note: If copy-pasting the key, it is better to paste it into the Web UI than the raw JSON config as the Web UI will escape double quotes automatically._
`shared_filesystem` | The shared filesystem to be used as a log directory. HDFS (`hdfs://hdfs-url`) and GCS (`gs://bucket-name`) are natively supported. This can also be a local file path (`/mnt/tensorflow`). If NFS is mounted at that local path, then all the files will be saved to the shared NFS. Otherwise it will only be saved locally.

### Using GCS

To use Google Cloud Storage bucket, first copy a service account key into the `gcs_key` option. Then set the `shared_filesystem` to a GCS address (e.g. `gs://bucket-name`).

### Mounting NFS

To use NFS instead of GCS or HDFS, one needs to install the NFS package in the container before mounting the folder. Since the DCOS Tensorflow package does not provide any way to run OS commands in the config, you have to do it through python.

The following python code can be run in the chief worker to mount an NFS locally.

```python
from subprocess import check_call

# Install required packages
check_call('apt-get update && apt-get --yes install nfs-common netbase', shell=True)
# Create the local directory location to mount the NFS
check_call('mkdir -p /mnt/nfs/tensorflow', shell=True)
# Mount the NFS folder locally at /mnt/nfs/tensorflow
check_call('mount 172.29.128.39:/srv/nfs/tensorflow /mnt/nfs/tensorflow', shell=True)
```

### Overview of files

Here are the files that are included in the `dcos_tensorflow_package` folder. Not all of them are important/useful, but I kept most of them just in case they are needed.

File | Description
---  | ---
`mnist_cpu_timed.py` | Distributed MNIST program. Doesn't specify CPU or GPU. Uses built-in Tensorflow MNIST data. Saves the total runtime into `time-completed.txt` in the __local run directory__.
`mnist_gpu_timed.py` | Distributed MNIST program. Forces GPU use when possible. Uses built-in Tensorflow MNIST data. Saves the total runtime into `time-completed.txt` in the __local run directory__.
`mnist_with_mount.py` | Distributed MNIST program. Doesn't specify CPU or GPU. Uses built-in Tensorflow MNIST data. <ul><li>NFS Address (hardcoded): `172.29.128.39:/srv/nfs/tensorflow`</li><li>Mount point (hardcoded): `/mnt/nfs/tensorflow` </li></ul>Installs NFS packages and mounts the NFS at the specified mount point. Uses the mount point as the log directory.
`mnist_with_mount_nfs_dataset.py` | Distributed MNIST program. Doesn't specify CPU or GPU. Uses dataset files in the NFS. (downloaded from <http://yann.lecun.com/exdb/mnist/>) <ul><li>NFS Address (hardcoded): `172.29.128.39:/srv/nfs/tensorflow`</li><li>Mount point: value of `shared_filesystem` option in the DC/OS config</li></ul> Reads MNIST data from the `dataset/` folder in the NFS (see code for the `data_dir` variable). Saves logs to the root of the NFS (see code for the `log_dir` variable).
`mnist-test-3-gpu-2-ps.json` | Launches `mnist_gpu_timed.py` with 3 GPU workers and 2 parameter servers. Uses GCS as the shared filesystem.
`mnist-with-mount.json` | MNIST config to launch `mnist_with_mount.py` or `mnist_with_mount_nfs_dataset.py`. Sets `/mnt/nfs/tensorflow` to be  the shared filesystem.

## Individually Created Containers

Instead of having the DC/OS Tensorflow package create all ther tasks automatically, we can create each worker/ps service individually.

### Pros

* Can execute OS commands directly
* Can mount host volumes in the container
* Can force placement on specific nodes

### Cons

* Need to create each task individually
* Starting the tasks required clicking "resume" invidiually on every single task
* Making modifications may be troublesome if all task configs needs to be updated
* Networking and ports has to be configured manually

### Using Google Cloud Storage

To use GCS, there needs to be a file containing the GCS Service Account key. You can simply put it in the repo, but that is insecure since the repo is public and anyone can download it.

Instead, I hardcoded the key into the DC/OS configs as the `GCS_KEY` environment variable. Then I wrote that value into a file when the container launches (`echo $GCS_KEY > /mnt/gcs_key.json`).

Tensorflow uses the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to determine the path to the key file. So I just had to set it to point to the key file I created at `/mnt/gcs_key.json` for Tensorflow to be able to authenticate.

### Implementation

To overcome the challenges with running 13 different nodes, I have come up with some ways to streamline the process.

1. The run commands are not placed directly in the "cmd" section of the service. Instead, all the "cmd" does is run a shell file (`run.sh` or `run_nondistributed.sh`) downloaded from the repo. This allows me to update the run commands across all tasks by simply changing a single file.
2. All the worker and ps services are placed inside a group. This allows me to stop all services in the group with one click. However, launching of each service still needs to be done individually. Groups are also useful for separating services from different runs.

#### Containers

* Worker services use the Mesos Universal Container Runtime (UCR), as `tensorflow-gpu` causes errors in Docker.
  * No container image is used with UCR
  * Since no container image is used, `tensorflow-gpu` has to be installed on every worker before running to be safe.
  * UCR can technically use `tensorflow-gpu` installed on the host, but this is not recommended as we want to ensure they all use the same version
* Parameter Server services use the `tensorflow/tensorflow` image in a Docker container
  * Parameter servers don't need the GPU, so it doesn't cause errors in Docker
  * Since `tensorflow-gpu` does not need to be installed on a ps, the run script will only install `tensorflow-gpu` on a worker service

#### How to use

1. Create the services for all the workers and ps
   1. Copy-paste the default JSON into the web config first
   2. Update the id of the new service
   3. Change the networking options for port number and name to avoid clashes, especially since multiple workers will run on the same server (important!)
   4. Change the hostname constraint (if necessary)
   5. Change the `TASK_INDEX` environment variable
2. Click resume on every task to start them
3. Monitor the logs for any errors
   * If there are errors, fix them, and restart all the tasks if necessary
4. Once there are no more errors, just let it run till completion
5. Frequently check if it is finished. If it is, stop the whole group and save your data. __Services will autorestart after finishing unless you manually stop them.__
   * Containers of stopped/finished services will be deleted after a while so avoid saving anything into the container. Instead use some shared filesystem like NFS or GCS.

### Overview of Files

Here are the files for individually creating containers. The run scripts are in the root folder while the JSON configs for the different services are in the `container_configs` folder.

#### Root folder

This folder contains all the run files. All python scripts in this folder use the built-in MNIST data for training. 

None of the progras use the `main(server, log_dir, context)` function signature for the main function, so it does not use the `context` object to read passed-in arguments. Instead, it reads in command-line flags (e.g. `--learning_rate=0.0001`) using helper functions found in `tf.app.flags`. The flags are specified in the various `run_*.sh` files.

File | Description
---  | ---
`mnist_distributed_gpu.py` | Distributed MNIST program I adapted from `mnist_distributed_example.py` and `mnist_gpu_timed`. Forces GPU usage when possible. Saves the total runtime into `time-completed.txt` in the __local run directory__.
`mnist_distributed.py` | Distributed MNIST program. Same as `mnist_distributed_gpu.py` in all aspects except that it doesn't force GPU usage.
`mnist_nondistributed_gpu.py` | Non-distributed MNIST program. Same as `mnist_distributed_gpu.py` in all other aspects. Forces GPU usage when possible.
`mnist_distributed_example.py` | Original distributed MNIST program I found online at <https://github.com/tensorflow/ecosystem/blob/master/docker/mnist.py>.
`run_nondistributed.sh` | Script to run `mnist_nondistributed_gpu.py`.
`run.sh` | Script to launch a worker/ps instance for `mnist_distributed_gpu.py`. The job name (either worker or ps) and task index are set using environment variables in the DC/OS service config. Currently specifies 2 PS and 11 workers (can be changed by adding/removing URLS). Also hardcodes the learning rate, max steps, log directory (GCS as of now), and run name (name of the folder where the summaries will be saved).

#### Container configs folder

All of the files in `container_configs` are for configuring the DC/OS services.

File | Description
---  | ---
`groups.json` | Config file for the whole mnist-gpu group of workers and parameter servers
`mnist-nondistributed.json` | For running the non-distributed MNIST program
`mnist-ps-*.json` | For running a parameter server in the mnist-gpu group. Change "mnist-gpu" to another name to run it in another group.
`mnist-worker-*.json` | For running a worker in the mnist-gpu group. Change "mnist-gpu" to another name to run it in another group.
`tensorboard-nfs.json` | Runs tensorboard using the host directory `/mnt/nfs/tensorflow` as the log directory.
`tensorboard.json` | Runs tensorboard using a GCS log directory. It first echos the Google Service Account key to a file and then runs tensorboard.

### Future Work

It is possible to further automate the deployment of the different workers and ps. For example, you can use a templating engine like Jinja to populate a pre-made config file for an entire group. You just have to set the different options, run the templating engine, and send the completed config file to the server for everything to start running automatically.

See <https://github.com/tensorflow/ecosystem/tree/master/marathon> for an implementation of this.