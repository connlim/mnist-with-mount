# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import print_function

import math
import os
import time
import datetime
import sys

import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

flags = tf.app.flags

# Training related flags
flags.DEFINE_string("data_dir", '/tmp',
                    "Directory where the mnist data is stored")
flags.DEFINE_string("log_dir", None,
                    "Directory for storing the checkpoints")
flags.DEFINE_string("run_name", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),
                    "Directory for storing the checkpoints")
flags.DEFINE_bool("fake_data", False,
                  "Whether to generate fake data")         
flags.DEFINE_integer("batch_size", 100, "Training batch size")
flags.DEFINE_integer("max_steps", 1000, "Max steps")
flags.DEFINE_float("learning_rate", 0.01, "Learning rate")
flags.DEFINE_float("dropout", 0.9, "Dropout thing??")

FLAGS = flags.FLAGS


def train(device):

    # Set FLAGS
    log_dir = FLAGS.log_dir
    fake_data = FLAGS.fake_data
    max_steps = FLAGS.max_steps
    learning_rate = FLAGS.learning_rate
    dropout = FLAGS.dropout
    data_dir = FLAGS.data_dir
    run_name = FLAGS.run_name

    # Import data
    mnist = input_data.read_data_sets(data_dir,
                                      one_hot=True,
                                      fake_data=fake_data)

    # Create a multilayer model.
    with tf.device(device):
        # Input placeholders
        with tf.name_scope('input'):
            x = tf.placeholder(tf.float32, [None, 784], name='x-input')
            y_ = tf.placeholder(tf.float32, [None, 10], name='y-input')

        with tf.name_scope('input_reshape'):
            image_shaped_input = tf.reshape(x, [-1, 28, 28, 1])
            tf.summary.image('input', image_shaped_input, 10)

        # We can't initialize these variables to 0 - the network will get stuck.
        def weight_variable(shape):
            """Create a weight variable with appropriate initialization."""
            initial = tf.truncated_normal(shape, stddev=0.1)
            return tf.Variable(initial)

        def bias_variable(shape):
            """Create a bias variable with appropriate initialization."""
            initial = tf.constant(0.1, shape=shape)
            return tf.Variable(initial)

        def variable_summaries(var):
            """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
            with tf.name_scope('summaries'):
                mean = tf.reduce_mean(var)
                tf.summary.scalar('mean', mean)
                with tf.name_scope('stddev'):
                    stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                tf.summary.scalar('stddev', stddev)
                tf.summary.scalar('max', tf.reduce_max(var))
                tf.summary.scalar('min', tf.reduce_min(var))
                tf.summary.histogram('histogram', var)

        def nn_layer(input_tensor, input_dim, output_dim, layer_name, act=tf.nn.relu):
            """Reusable code for making a simple neural net layer.

            It does a matrix multiply, bias add, and then uses ReLU to nonlinearize.
            It also sets up name scoping so that the resultant graph is easy to read,
            and adds a number of summary ops.
            """
            # Adding a name scope ensures logical grouping of the layers in the graph.
            with tf.name_scope(layer_name):
                # This Variable will hold the state of the weights for the layer
                with tf.name_scope('weights'):
                    weights = weight_variable([input_dim, output_dim])
                    variable_summaries(weights)
                with tf.name_scope('biases'):
                    biases = bias_variable([output_dim])
                    variable_summaries(biases)
                with tf.name_scope('Wx_plus_b'):
                    preactivate = tf.matmul(input_tensor, weights) + biases
                    tf.summary.histogram('pre_activations', preactivate)
                activations = act(preactivate, name='activation')
                tf.summary.histogram('activations', activations)
                return activations

        hidden1 = nn_layer(x, 784, 500, 'layer1')

        with tf.name_scope('dropout'):
            keep_prob = tf.placeholder(tf.float32)
            tf.summary.scalar('dropout_keep_probability', keep_prob)
            dropped = tf.nn.dropout(hidden1, keep_prob)

        # Do not apply softmax activation yet, see below.
        y = nn_layer(dropped, 500, 10, 'layer2', act=tf.identity)

        with tf.name_scope('cross_entropy'):
            # The raw formulation of cross-entropy,
            #
            # tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.softmax(y)),
            #                               reduction_indices=[1]))
            #
            # can be numerically unstable.
            #
            # So here we use tf.nn.softmax_cross_entropy_with_logits on the
            # raw outputs of the nn_layer above, and then average across
            # the batch.
            diff = tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y)
            with tf.name_scope('total'):
                cross_entropy = tf.reduce_mean(diff)
        tf.summary.scalar('cross_entropy', cross_entropy)

        with tf.name_scope('train'):
            global_step = tf.train.get_or_create_global_step()
            train_step = tf.train.AdamOptimizer(learning_rate).minimize(
                cross_entropy, global_step=global_step)

        with tf.name_scope('accuracy'):
            with tf.name_scope('correct_prediction'):
                correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
            with tf.name_scope('accuracy'):
                accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar('accuracy', accuracy)

        # Merge all the summaries and write them out to
        # /tmp/tensorflow/mnist/logs/mnist_with_summaries (by default)
        merged = tf.summary.merge_all()

    # Set up Session
    hooks = [tf.train.StopAtStepHook(last_step=max_steps)]
    is_chief = True

    # Create the file writers for the train and test runs. 
    # Set flush_secs to 99999999 so that the file writers do not flush the data till
    # they are closed. This prevents the flushing from affecting the time taken 
    train_writer = tf.summary.FileWriter(log_dir + '/train-' + run_name, 
                                         graph=tf.get_default_graph(),
                                         flush_secs=99999999)
    test_writer = tf.summary.FileWriter(log_dir + '/test-' + run_name,
                                        flush_secs=99999999)
    
    # allow_soft_placement ensures that every op that can use the GPU
    # will use it, while every op that does not support GPU will use CPU
    with tf.train.MonitoredTrainingSession(is_chief=is_chief,
                                           hooks=hooks,
                                           config=tf.ConfigProto(allow_soft_placement=True,
                                                                 log_device_placement=True)) as sess:

        # Train the model, and also write summaries.
        # Every 10th step, measure test-set accuracy, and write test summaries
        # All other steps, run train_step on training data, & add training summaries

        def feed_dict(train):
            """Make a TensorFlow feed_dict: maps data onto Tensor placeholders."""
            if train or fake_data:
                xs, ys = mnist.train.next_batch(100, fake_data=fake_data)
                k = dropout
            else:
                xs, ys = mnist.test.images, mnist.test.labels
                k = 1.0
            return {x: xs, y_: ys, keep_prob: k}

        i = 0
        while not sess.should_stop():
            # Get the current global step
            gstep = tf.train.global_step(sess, global_step)
            
            # Check again if the session has stopped. 
            # This prevents the error "Run called even after should_stop requested" that happens
            # when sess.run() is called after getting the global step.
            if sess.should_stop():
                break

            if i % 10 == 0:  # Record summaries and test-set accuracy
                summary, acc = sess.run([merged, accuracy], feed_dict=feed_dict(False))
                if is_chief:
                    test_writer.add_summary(summary, gstep)
                print('Accuracy at local step %s: %s' % (i, acc))
                print('global_step: %s' % gstep)
                # test_writer.flush()
            else:  # Record train set summaries, and train 
                if i % 100 == 99:  # Record execution stats
                    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                    run_metadata = tf.RunMetadata()
                    summary, _ = sess.run([merged, train_step],
                                          feed_dict=feed_dict(True),
                                          options=run_options,
                                          run_metadata=run_metadata)
                    if is_chief:
                        train_writer.add_run_metadata(run_metadata, 'step%03d' % gstep, global_step=gstep)
                        train_writer.add_summary(summary, global_step=gstep)
                        print('Adding run metadata for', gstep)
                        # train_writer.flush()
                else:  # Record a summary
                    summary, _ = sess.run([merged, train_step], feed_dict=feed_dict(True))
                    if is_chief:
                        train_writer.add_summary(summary, gstep)
            i += 1
            sys.stdout.flush()
    if is_chief:
        # Flush and close file writers after training is finished
        train_writer.close()
        test_writer.close()
        print('Closed train and test writer')


def main(unused_argv):
    if FLAGS.data_dir is None or FLAGS.data_dir == "":
        raise ValueError("Must specify an explicit `data_dir`")
    if FLAGS.log_dir is None or FLAGS.log_dir == "":
        raise ValueError("Must specify an explicit `log_dir`")

    start = time.time()

    train('/device:GPU:0') # Use 1 GPU device to train

    end = time.time()
    with open("time-completed.txt", "w") as f:
        hours, rem = divmod(end-start, 3600)
        minutes, seconds = divmod(rem, 60)
        f.write("{:0>2}:{:0>2}:{:05.2f}\n".format(int(hours),int(minutes),seconds))


if __name__ == "__main__":
    tf.app.run()
