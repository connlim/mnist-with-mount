#!/usr/bin/env bash

# Saves the value of the $GCS_KEY environment variable into /mnt/gcs_key.json and sets $GOOGLE_APPLICATION_CREDENTIALS to use that file as the service account key. 
echo $GCS_KEY > /mnt/gcs_key.json
export GOOGLE_APPLICATION_CREDENTIALS="/mnt/gcs_key.json"

# Set up a virtualenv and install the latest version of tensorflow-gpu.
virtualenv --system-site-packages -p python2.7 ./venv
source ./venv/bin/activate  # sh, bash, ksh, or zsh
python -m pip install --upgrade tensorflow-gpu

# Run the program with various flags set
# Enable word wrap to read the command easier
python $FILE_PATH/mnist_nondistributed_gpu.py --log_dir=$LOG_DIR --learning_rate=0.0001 --max_steps=50000 --run_name=$RUN_NAME