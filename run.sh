#!/usr/bin/env bash

# Saves the value of the $GCS_KEY environment variable into /mnt/gcs_key.json and sets $GOOGLE_APPLICATION_CREDENTIALS to use that file as the service account key. 
echo $GCS_KEY > /mnt/gcs_key.json
export GOOGLE_APPLICATION_CREDENTIALS="/mnt/gcs_key.json"

# Only workers need to install tensorflow-gpu. ps uses the docker tensorflow container
if [ "$JOB_NAME" == "worker" ]
then
    # Set up a virtualenv and install the latest version of tensorflow-gpu.
    virtualenv --system-site-packages -p python2.7 ./venv
    source ./venv/bin/activate  # sh, bash, ksh, or zsh
    python -m pip install --upgrade tensorflow-gpu
fi

# Run the program with various flags set
# The hostnames of each worker/ps is hardcoded in the flags
# Enable word wrap to read the command easier
python $FILE_PATH/mnist_distributed_gpu.py --ps_hosts=mnist-gpumnist-ps-0.marathon.l4lb.thisdcos.directory:2223,mnist-gpumnist-ps-1.marathon.l4lb.thisdcos.directory:2224 --worker_hosts=mnist-gpumnist-worker-0.marathon.l4lb.thisdcos.directory:2250,mnist-gpumnist-worker-1.marathon.l4lb.thisdcos.directory:2251,mnist-gpumnist-worker-2.marathon.l4lb.thisdcos.directory:2252,mnist-gpumnist-worker-3.marathon.l4lb.thisdcos.directory:2253,mnist-gpumnist-worker-4.marathon.l4lb.thisdcos.directory:2254,mnist-gpumnist-worker-5.marathon.l4lb.thisdcos.directory:2255,mnist-gpumnist-worker-6.marathon.l4lb.thisdcos.directory:2256,mnist-gpumnist-worker-7.marathon.l4lb.thisdcos.directory:2257,mnist-gpumnist-worker-8.marathon.l4lb.thisdcos.directory:2258,mnist-gpumnist-worker-9.marathon.l4lb.thisdcos.directory:2259,mnist-gpumnist-worker-10.marathon.l4lb.thisdcos.directory:2260 --job_name=$JOB_NAME --task_index=$TASK_INDEX --log_dir=gs://dso-mnist-tests --learning_rate=0.0001 --max_steps=50000 --run_name=11-gpus

# List of worker hosts, for backup reasons

#mnist-gpumnist-worker-0.marathon.l4lb.thisdcos.directory:2250,mnist-gpumnist-worker-1.marathon.l4lb.thisdcos.directory:2251,mnist-gpumnist-worker-2.marathon.l4lb.thisdcos.directory:2252,mnist-gpumnist-worker-3.marathon.l4lb.thisdcos.directory:2253,mnist-gpumnist-worker-4.marathon.l4lb.thisdcos.directory:2254,mnist-gpumnist-worker-5.marathon.l4lb.thisdcos.directory:2255,mnist-gpumnist-worker-6.marathon.l4lb.thisdcos.directory:2256,mnist-gpumnist-worker-7.marathon.l4lb.thisdcos.directory:2257,mnist-gpumnist-worker-8.marathon.l4lb.thisdcos.directory:2258,mnist-gpumnist-worker-9.marathon.l4lb.thisdcos.directory:2259,mnist-gpumnist-worker-10.marathon.l4lb.thisdcos.directory:2260